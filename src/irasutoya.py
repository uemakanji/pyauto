from selenium import webdriver
import os
import time
import sys
import urllib.request

driver = webdriver.Chrome(os.path.abspath('../driver/chromedriver'))

driver.get('https://www.irasutoya.com/')

driver.find_element_by_id('searchwords').send_keys(sys.argv[1])
time.sleep(1)
driver.find_element_by_id('searchBtn').click()

driver.find_element_by_class_name(
    'boxim').find_element_by_tag_name('a').click()

img_url = driver.find_element_by_class_name(
    'separator').find_element_by_tag_name('img').get_attribute('src')
urllib.request.urlretrieve(img_url, 'test.jpg')