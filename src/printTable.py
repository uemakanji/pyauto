#! python3
# printTable.py - 配列を整えて出力する

table_data = [
    ['apples', 'oranges', 'bananas', 'cherries'],
    ['Alice', 'Bob', 'Carol', 'David'],
    ['dogs', 'cats', 'moose', 'goose']
]

def print_table():
    table_len = list()
    for items in table_data:
        max_len = 0
        for item in items:
            if len(item) > max_len:
                max_len = len(item)
        table_len.append(max_len)
    
    for i, j, k in zip(table_data[0], table_data[1], table_data[2]):
        print(i.rjust(table_len[0]),
            j.rjust(table_len[1]), k.rjust(table_len[2]))

print_table()