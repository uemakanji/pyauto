#! python3
# pw.py - パスワード管理プログラム(脆弱性あり)

PASSWORD = {
    'email': 'aokdaknfgakerl',
    'blog': 'AIfsodfolfLK99',
    'luggage': '12345'
}

import sys
import pyperclip

if len(sys.argv) < 2:
    print('使い方: python pw.py [アカウント名]')
    print('パスワードをクリップボードにコピーします')
    sys.exit()

account = sys.argv[1]

if account in PASSWORD:
    pyperclip.copy(PASSWORD[account])
    print(account + 'のパスワードをクリップボードにコピーしました')
else:
    print(account + 'というアカウントはありません')