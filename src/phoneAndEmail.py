#! python3
# phoneAndEmail.py - クリップボードから携帯電話とアドレスを検索する

import re
import pyperclip

# 電話番号の正規表現を作る
phone_regex = re.compile(r'''(
    (\d{1,4}|\(\d{1,4}\))
    (\s|-)
    (\d{1,4})
    (\s|-)
    (\d{3,4})
    (\s*(ext|x|ext.)\s*(\d{2,5}))?
    )''', re.VERBOSE)

# 電子メールの正規表現を作る
mail_regex = re.compile(r'''(
    [a-zA-Z0-9._%+-]+
    @
    [a-zA-Z0-9.-]+
    (\.[a-zA-Z]{2,4})
)''', re.VERBOSE)

# クリップボードのテキストを検索する
text = str(pyperclip.paste())
matches = []
for groups in phone_regex.findall(text):
    phone_num = '-'.join([groups[1], groups[3], groups[5]])
    if groups[8] != '':
        phone_num = phone_num + ' x' + groups[8]
    matches.append(phone_num)

for groups in mail_regex.findall(text):
    matches.append(groups[0])

# 検索結果をクリップボードに貼り付ける
if len(matches) > 0:
    pyperclip.copy('\n'.join(matches))
    print("クリップボードにコピーしました")
    print('\n'.join(matches))
else:
    print("見つかりませんでした")
