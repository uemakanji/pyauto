from selenium import webdriver
import os
import time
import sys

# 操作するブラウザを開く
driver = webdriver.Chrome(os.path.abspath('../driver/chromedriver'))

# 操作するページを開く
driver.get('https://www.google.com/')

# 操作する
driver.find_element_by_name('q').send_keys(sys.argv[1])
time.sleep(1)
driver.find_element_by_class_name(
    'aajZCb').find_element_by_name('btnK').click()
