#! python3
# bulletPointAdder.py クリップボードの各テキストに
# *を打って、箇条書きにする

import pyperclip

text = pyperclip.paste()
lines = text.split('\n')

for i in range(len(lines)):
    lines[i] = '* ' + lines[i]

text = '\n'.join(lines)
pyperclip.copy(text)